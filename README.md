<h1>System obsługi wind</h1>

<h1>Uruchomienie (używam JDK15)</h1>
<h3>gradle execute</h3>

<h1>Instrukacja</h1>
<h4>Dostepne komendy:</h4>

                1. Aby użyskać pomoc : -help

                2. Przywołanie windy : -pickup liftId floor direct
                  liftId - id windy
                  floor - numer piętra na którym się znajdujesz
                  direct - kierunek : UP - góra, DOWN - dół

                3. Symulowanie jednego kroku windy : -simulate liftId
                  liftId - id windy

                4. Generowanie statusu windy : -status liftId
                  liftId - id windy

                5. Wylaczenie programu : -exit

<h4>Opis poszczególnych klas / metod jest wewnątrz kodu.</h4>
