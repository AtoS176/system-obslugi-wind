import algorithm.LiftProvider;
import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        //wyswietl instrukcje
        System.out.println(Message.help());
        System.out.println(Message.addOrderInstructions());
        System.out.println(Message.simulateInstructions());
        System.out.println(Message.statusInstructions());
        System.out.println(Message.exitInstructions());

        //powitalna wiadomosc
        System.out.println(Message.helloMessage());

        //wczytaj liczbe wind
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Try<Integer> readLine = Try.of(() -> Integer.valueOf(br.readLine()));

        while(readLine.isFailure()){
            System.out.println(Message.validLiftNumber());
            readLine = Try.of(() -> Integer.valueOf(br.readLine()));
        }

        //stworzenie ListProvidera do obsługi wind
        final Integer liftNumber = readLine.get();
        LiftProvider liftProvider = new LiftProvider(liftNumber);

        //zarejestrowana liczba wind
        //Jeśli podasz np. 4 windy, to windy otrzymają kolejno ID : 1,2,3,4
        System.out.println(Message.registered(liftNumber));

        //stworzenia parsera do odczytywania danych z CLI
        InputParser inputParser = new InputParser(liftProvider);

        //odczytywanie danych z CLI
        while(true){
            Try<String> readCommand = Try.of(br::readLine);
            readCommand.peek(inputParser::parse);
        }

    }

}
