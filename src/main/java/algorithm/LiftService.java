package algorithm;

import io.vavr.Tuple2;

import java.util.List;

/**
 * Interfejs definiujący operacje, ktore możemy wykonac na windzie.
 *
 * PS. Nie ma tu metody update, bo aktulizacja kolejki (dodanie/usuwanie)
 * jest wykonywana automatycznie wraz z wykonaniem addOrder, addTargetFloor oraz simulate.
 *
 * Metoda findAndUpdate to metoda prywatna w klasie Lift
 *
 *
 */
public interface LiftService {

    /**
     * Przywołanie windy.
     *
     * @param order
     */
    void addOrder(Order order);


    /**
     * Wybranie pietra docelowego. Mamy możliwość wykonania tej akcji, kiedy wejdziemy do windy.
     *
     * @param floor piętro docelowe
     */
    void addTargetFloor(int floor);


    /**
     * Symulacja jednego kroku windy.
     *
     * @return krotka zawierająca pietro startowe oraz pietro do którego dotarła winda
     */
    Tuple2<Integer, Integer> simulate();


    /**
     * Generowania aktualnego statusu windy.
     *
     * @return lista krotek, każda krotka zawiera pietro startowe oraz pietro koncowe.
     *         Krotki są w takiej kolejnosci, w jakiej winda bedzie jezdzic.
     */
    List<Tuple2<Integer, Integer>> createStatusList();
}