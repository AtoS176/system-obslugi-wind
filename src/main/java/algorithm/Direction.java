package algorithm;

/**
 * Enum do okreslania kierunku jazdy.
 */
public enum Direction {
    DOWN(-1),
    UP(1);

    public int value;

    Direction(int value) {
        this.value = value;
    }

}
