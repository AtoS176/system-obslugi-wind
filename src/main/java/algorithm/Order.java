package algorithm;

/**
 * Klasa która reprezentuje przywoływanie windy przez daną osobę.
 * Jeśli przywołujemy winde, to tworzymy ten obiekt.
 * Z racji tego, ze jest to zwykłe DTO, które nic nie robi,
 * to nie korzystam z enkapsulacji.
 */
public class Order {
    public final Direction direction;
    public final int liftId;
    public final int floor;

    public Order(Direction direction, int liftId, int floor) {
        this.direction = direction;
        this.liftId = liftId;
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Order{" +
                "direction=" + direction +
                ", liftId=" + liftId +
                ", floor=" + floor +
                '}';
    }
}
