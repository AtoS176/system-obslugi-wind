package algorithm;

import io.vavr.Tuple;
import io.vavr.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class Lift implements LiftService {
    private final int id;
    private int actualFloor;
    private List<Integer> ordersQueue;

    public Lift(int id) {
        this.id = id;
        this.actualFloor = 0;
        this.ordersQueue = new ArrayList<>();
    }

    @Override
    public void addOrder(Order order) {

        //1. Jeśli w kolejce jest już dane piętro to ignorujemy
        if (ordersQueue.contains(order.floor)) {
            return;
        }

        //2. Jesli kolejka jest pusta, to po prostu wstawiamy
        if (ordersQueue.isEmpty()) {
            ordersQueue.add(order.floor);
            return;
        }

        //3. Jeśli w kolejce coś jest to szukamy takiej pary : <piętro startowe, następne piętro>
        // Aby aktualnie dodawane piętro znalazło się między nimi

        boolean isDirectUp = Direction.UP.equals(order.direction);
        findAndUpdate(isDirectUp, order.floor);
    }

    @Override
    public void addTargetFloor(int floor) {
        //Jeśli klient wchodzi do windy, tzn. że actualFloor == piętro na którym wchodzi klient.
        //Czyli jeśli poda floor mniejsze od aktulnego tzn. ze jedzie w dół.

        //W zasadzie algorytm jest taki sam jak dla metody addOrder,
        //tylko w tym przypadku klient podaje pietro docelowe.
        //Ale mamy wystarczającą liczbę informacji, aby skonstruować obiekt Order.

        Direction direction = floor > actualFloor ? Direction.UP : Direction.DOWN;
        addOrder(new Order(direction, this.id, floor));
    }

    @Override
    public Tuple2<Integer, Integer> simulate() {
        Tuple2<Integer, Integer> transit = Tuple.of(actualFloor, ordersQueue.get(0));
        actualFloor = ordersQueue.get(0);
        ordersQueue.remove(0);
        return transit;
    }

    @Override
    public List<Tuple2<Integer, Integer>> createStatusList() {
        List<Tuple2<Integer, Integer>> statusList = new ArrayList<>();
        int firstStop = actualFloor;

        for (int floor : ordersQueue) {
            statusList.add(Tuple.of(firstStop, floor));
            firstStop = floor;
        }

        return statusList;
    }

    /**
     * Znajduje optymalne miejsce w kolejce dla nowego pietra.
     * Szukamy takiej pary dwóch kolejnych pięter : (pietro1, pietro2)
     * Aby nowe piętro znalazło się między nimi.
     * Dzięki temu wstawiane piętro będzie znajdować sie 'po drodze'
     * Przez co nie będziemy musieli tracić dodatkowego czasu na dojazd.
     * Musimy rozpatrzeć dwa przypadki w zaleznosci od tego czy klient chce
     * jechac w gore czy w dol.
     *
     * @param isDirectUp czy klient chce jechac w gore
     * @param floor pietro na ktorym ma sie zatrzymac winda
     */
    private void findAndUpdate(boolean isDirectUp, int floor){
        int firstFloor = actualFloor;

        for (int i = 0; i < ordersQueue.size(); i++) {
            int secondFloor = ordersQueue.get(i);
            Direction liftDirection = secondFloor > firstFloor ? Direction.UP : Direction.DOWN;

            boolean whenDirectUP = false;
            boolean whenDirectDOWN = false;

            //Jesli winda jedzie do gory, i klient tez chce jechac do gory to szukamy miejsca aby wstawic
            //go jako przystanek posredni
            if(Direction.UP.equals(liftDirection)){
                whenDirectUP = floor > firstFloor && floor < secondFloor && isDirectUp;
            }

            //Analogicznie jak wyzej tylko dla kierunku DOWN
            if(Direction.DOWN.equals(liftDirection)){
                whenDirectDOWN = floor > secondFloor && !isDirectUp;
            }

            if (whenDirectUP|| whenDirectDOWN) {
                ordersQueue.add(i, floor);
                return;
            }

            firstFloor = secondFloor;
        }

        //Jesli nie znalezlismy odpowidniej pary, to dodajemy na koniec.
        //Czyli jesli nie znajdziemy przedziału, to wtedy przywołania są realizowane
        //wedlug kolejnosci zglaszania
        ordersQueue.add(floor);
    }

}
