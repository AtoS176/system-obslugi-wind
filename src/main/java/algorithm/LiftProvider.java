package algorithm;

import io.vavr.Tuple2;

import java.util.HashMap;
import java.util.List;

/**
 * Klasa do obsługi wind. Wywołuje metody z interfejsu LiftService dla windy o konkretnym id.
 */
public class LiftProvider {
    private final HashMap<Integer, Lift> liftMap = new HashMap<>();

    public LiftProvider(int liftNumber) {
        for(int i = 1; i <= liftNumber; i++){
            liftMap.put(i, new Lift(i));
        }
    }

    public void addOrder(int liftId, Order order){
        liftMap.get(liftId).addOrder(order);
    }


    public void addTargetFloor(int liftId, int floor){
        liftMap.get(liftId).addTargetFloor(floor);
    }

    public Tuple2<Integer, Integer> simulate(int liftId){
        return liftMap.get(liftId).simulate();
    }


    public List<Tuple2<Integer, Integer>> createStatusList(int liftId){
        return liftMap.get(liftId).createStatusList();
    }

}
