import algorithm.Direction;
import algorithm.LiftProvider;
import algorithm.Order;
import io.vavr.Tuple2;
import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Logger;

/**
 * Klasa do parsowania polecen klienta wpisanych do konsoli.
 */
public class InputParser {

    public enum Command{
        HELP("-help"),
        PICKUP("-pickup"),
        SIMULATE("-simulate"),
        STATUS("-status"),
        EXIT("-exit");

        public final String text;

        Command(String text) {
            this.text = text;
        }

    }

    private final static Logger LOGGER = Logger.getLogger(InputParser.class.getName());
    private final LiftProvider liftProvider;

    public InputParser(LiftProvider liftProvider) {
        this.liftProvider = liftProvider;
    }

    /**
     * Odczytywanie polecen klienta wpisywanych do konsoli.
     *
     * @param message wpisane polecenie
     */
    public void parse(String message){
        String[] splitBySpace = message.split(" ");
        String command = splitBySpace[0];

        //Funkcja help
        if (Command.HELP.text.equals(command)) {
            System.out.println(Message.addOrderInstructions());
            System.out.println(Message.simulateInstructions());
            System.out.println(Message.statusInstructions());
            System.out.println(Message.exitInstructions());
        }

        //Przywolanie windy
        else if(Command.PICKUP.text.equals(command)){

            if(splitBySpace.length != 4){
                System.out.println(Message.wrongArgs());
                return;
            }

            int liftId = Integer.parseInt(splitBySpace[1]);
            int floor = Integer.parseInt(splitBySpace[2]);
            Direction direction = Direction.valueOf(splitBySpace[3]);
            Order order = new Order(direction, liftId, floor);
            liftProvider.addOrder(liftId, order);
            LOGGER.info("ADD order " + order.toString());
        }

        //Symulowanie jednego kroku windy. Po przemieszczeniu sie windy,
        // winda zatrzymuje za na danym pietrze,
        // - jesli klient wchodzi to podaje pietro docelowe
        // - jesli klient wychodzi to klika enter
        else if(Command.SIMULATE.text.equals(command)){
            if(splitBySpace.length != 2){
                System.out.println(Message.wrongArgs());
                return;
            }

            int liftId = Integer.parseInt(splitBySpace[1]);
            Tuple2<Integer, Integer> transit = liftProvider.simulate(liftId);

            //Wypisz wynik symulacji
            System.out.println(Message.simulateMessage(transit));
            System.out.println(Message.addFloorTargetMessage(transit._2));

            //Jesli wchodzi wpisz pietro docelowe, jesli wychodzisz kliknij enter.
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            Try<String> clientMessage = Try.of(reader::readLine);

            if(clientMessage.isSuccess()){

                //wychodzimy z windy
                if(clientMessage.get().isEmpty()){
                    LOGGER.info("Simulate : klient wyszedl z windy.");
                    return;
                }

                //klient wszedl i podaje pietro docelowe
                int targetFloor = Integer.parseInt(clientMessage.get());
                liftProvider.addTargetFloor(liftId, targetFloor);
                LOGGER.info("ADD message; liftId: " + liftId + " target: " + targetFloor);
            }
        }

        //Sprawdzenie statusu windy
        else if(Command.STATUS.text.equals(command)){
            if(splitBySpace.length != 2){
                System.out.println(Message.wrongArgs());
                return;
            }

            int liftId = Integer.parseInt(splitBySpace[1]);
            List<Tuple2<Integer, Integer>> transit = liftProvider.createStatusList(liftId);
            System.out.println(Message.statusInfo(transit));
        }

        //wyjscie z systemu
        else if(Command.EXIT.text.equals(command)){
            System.exit(0);
        }

        //Nie rozpoznano komendy
        else{
            System.out.println(Message.unknownCommand());
        }

    }

}
