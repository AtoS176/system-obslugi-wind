import io.vavr.Tuple2;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Klasa ktora pelni role pojemnika, ktory przechwouje wiadomosci wysylane do klienta.
 */
public class Message {
    public static String help(){
        return "Instrukcja. Aby użyskać pomoc : -help\n";
    }

    public static String addOrderInstructions(){
        return """
                1. Przywołanie windy : -pickup liftId floor direct
                  liftId - id windy
                  floor - numer piętra na którym się znajdujesz
                  direct - kierunek : UP - góra, DOWN - dół
                """;
    }

    public static String simulateInstructions(){
        return """
                2. Symulowanie jednego kroku windy : -simulate liftId
                  liftId - id windy
                """;
    }

    public static String statusInstructions(){
        return """
                3. Generowanie statusu windy : -status liftId
                  liftId - id windy
                """;
    }

    public static String exitInstructions(){
        return "4. Wylaczenie programu : -exit \n";
    }

    public static String helloMessage(){
        return "Witaj w systemie obslugi wind. Podaj liczbe wind, która chcesz obsłużyć:";
    }

    public static String validLiftNumber(){
        return "Podana wartosc musi byc liczbą naturalna . Wprowadz dane ponownie:";
    }

    public static String simulateMessage(Tuple2<Integer, Integer> transit){
        return "Winda przemiescila sie z pietra " + transit._1 + " do pietra " + transit._2;
    }

    public static String statusInfo(List<Tuple2<Integer, Integer>> status){
        return status.stream()
                .map(pair -> "Pietro startowe : " + pair._1 + ", Pietro koncowe: " + pair._2 + "\n")
                .collect(Collectors.joining());
    }

    public static String addFloorTargetMessage(int floor){
        return "Winda zatrzymala sie na " + floor + " pietrze. Jesli wysiadasz kliknij enter, wpp wpisz numer pietra :";
    }

    public  static String unknownCommand(){
        return "Nie rozpoznano komendy. Uzyj -help, aby zobaczyc mozliwe komendy.";
    }

    public static String registered(int liftNumber){
        return "System zostal uruchomiony dla " + liftNumber + " windy/wind. Mozesz wpisywac komendy.";
    }

    public static String wrongArgs(){
        return "Podales bledna liczbe argumentow.";
    }

}
