package algorithm;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LiftTest {
    final int liftId = 1;
    private final Lift lift = new Lift(liftId);

    @Test
    public void addOrder_betweenOrder_whenDirectionUP(){
        //given
        Order first = new Order(Direction.UP, liftId, 2);
        Order second = new Order(Direction.UP, liftId, 5);
        Order third = new Order(Direction.UP, liftId, 3);

        //when
        lift.addOrder(first);
        lift.addOrder(second);
        lift.addOrder(third);

        Tuple2<Integer, Integer> shouldBeFirst = lift.simulate();
        Tuple2<Integer, Integer> shouldBeThird = lift.simulate();
        Tuple2<Integer, Integer> shouldBeSecond = lift.simulate();

        //then
        Assertions.assertEquals(Tuple.of(0, 2), shouldBeFirst);
        Assertions.assertEquals(Tuple.of(2, 3), shouldBeThird);
        Assertions.assertEquals(Tuple.of(3, 5), shouldBeSecond);
    }

    @Test
    public void addOrder_betweenOrder_whenDirectionDOWN(){
        //given
        Order first = new Order(Direction.DOWN, liftId, 10);
        Order second = new Order(Direction.DOWN, liftId, 5);
        Order third = new Order(Direction.DOWN, liftId, 8);

        //when
        lift.addOrder(first);
        lift.addOrder(second);
        lift.addOrder(third);

        Tuple2<Integer, Integer> shouldBeFirst = lift.simulate();
        Tuple2<Integer, Integer> shouldBeThird = lift.simulate();
        Tuple2<Integer, Integer> shouldBeSecond = lift.simulate();

        //then
        Assertions.assertEquals(Tuple.of(0, 10), shouldBeFirst);
        Assertions.assertEquals(Tuple.of(10, 8), shouldBeThird);
        Assertions.assertEquals(Tuple.of(8, 5), shouldBeSecond);
    }

    @Test
    public void addOrder_betweenOrder_whenDirectionMIXED(){
        //given
        Order first = new Order(Direction.DOWN, liftId, 10);
        Order second = new Order(Direction.DOWN, liftId, 5);
        Order third = new Order(Direction.UP, liftId, 6);
        Order fourth = new Order(Direction.UP, liftId, 8);
        Order fifth = new Order(Direction.UP, liftId, 7);

        //when
        lift.addOrder(first);
        lift.addOrder(second);
        lift.addOrder(third);
        lift.addOrder(fourth);
        lift.addOrder(fifth);

        Tuple2<Integer, Integer> shouldBeFirst = lift.simulate();
        Tuple2<Integer, Integer> shouldBeSecond = lift.simulate();
        Tuple2<Integer, Integer> shouldBeThird = lift.simulate();
        Tuple2<Integer, Integer> shouldBeFifth = lift.simulate();
        Tuple2<Integer, Integer> shouldBeFourth = lift.simulate();

        //Komenatrz
        //Na poczatku realizujemy sekwencje polaczen w gore tj. 6 -> 7 > 8
        //Na koncu realizujemy sekwencje polaczen w dol tzn : 10 > 5

        //then
        Assertions.assertEquals(Tuple.of(0, 6), shouldBeFirst);
        Assertions.assertEquals(Tuple.of(6, 7), shouldBeSecond);
        Assertions.assertEquals(Tuple.of(7, 8), shouldBeThird);
        Assertions.assertEquals(Tuple.of(8, 10), shouldBeFifth);
        Assertions.assertEquals(Tuple.of(10, 5), shouldBeFourth);
    }

}
